import { useState } from "react";
import Navbar from "./components/Navbar";
import Sidebar from "./components/Sidebar";
import Board from "./components/Boards";
import * as React from 'react'
import Home from "./components/Home";
import { ChakraProvider } from '@chakra-ui/react'
import{BrowserRouter as Router ,Route,Switch} from 'react-router-dom'
function App() {
  let [sidemenu,setSidemenu]=useState(false)
  return (
    <Router>
    <ChakraProvider>
      <div >
      <Navbar sidemenu={sidemenu} setSidemenu={setSidemenu}/>
      <Switch>
      
    
      <Route exact path='/'>
      <Home/>
      </Route>
      {/* <Board/> */}
      
      <Route path='/board/:id' component={Board}>
      </Route>
      </Switch>
    </div>
    </ChakraProvider>
    </Router>
  )
}

export default App;
