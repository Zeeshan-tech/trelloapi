import React, { Component } from "react";
import { Button } from "@chakra-ui/react";
export default class AddChecklist extends Component {
  render() {
    return (
      <div>
        <Button
          size="md"
          height="48px"
          width="200px"
          border="2px"
          borderColor="green.500"
        >
          create Checklist
        </Button>
      </div>
    );
  }
}
