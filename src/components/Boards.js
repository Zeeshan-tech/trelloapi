import React, { Component } from "react";
import {
  VStack,
  HStack,
  Box,
  Heading,
  Flex,
  Text,
  StackDivider,
} from "@chakra-ui/react";
import { Spinner,Button } from "@chakra-ui/react";
import { makeList, getLists, deleteList } from "../api";
import DisplayCards from "./DisplayCards";
import { DeleteIcon } from '@chakra-ui/icons'

export default class Boards extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loader: true,
      listname: [],
      listdata: "",
    };
  }
  componentDidMount = () => {
    let board=(this.props.match.params['id'])
    getLists(board)
      .then((lists) => {
        console.log(lists);
        this.setState({
          listname: lists,
          loader: false,
        });
      })
      .catch((err) => console.error(err));
  };
  handleChange = (e) => {
    this.setState({
      ...this.state,
      listdata: e.target.value,
    });
  };
  handlesubmit = () => {
    let { listdata } = this.state;
    let board=(this.props.match.params['id'])
    makeList(listdata,board).then(()=>{
      getLists(board)
      .then((lists) => {
        console.log(lists,'...listss');
        this.setState({
          listname: lists,
          loader: false,
        });
      })
    }).catch((err) => console.error(err));

  };
  handleListDel=(id)=>{
    deleteList(id).then(()=>{
      let board=(this.props.match.params['id'])
    getLists(board)
      .then((lists) => {
        console.log(lists);
        this.setState({
          listname: lists,
          loader: false,
        });
      })
      .catch((err) => console.error(err));
    })
  }
  render() {
    let { loader, listname, listdata } = this.state;
    if (loader) {
      return (
        <Spinner
          thickness="4px"
          speed="0.65s"
          emptyColor="gray.200"
          color="blue.500"
          size="xl"
        />
      );
    }
    return (
      <Box overflow={'scroll'}>
        <Flex>
          {listname.map((list) => (
            <Box p={2} ml={5} minW="300" boxShadow='2xl' bg="lightgray">
              <Flex align="center" justify="space-between" mb="30px">
                <Heading size="md">{list.name}</Heading>
                <Button m={0} fontSize="4xl" onClick={()=>this.handleListDel(list.id)}>
                <DeleteIcon w={6} h={6} />
                </Button>
              </Flex>
              <DisplayCards id={list.id} />
            </Box>
          ))}
          <Box minW="300px" p={2}  border='.8px solid grey'>
            <input
              placeholder="create a new list"
              type="text"
              value={listdata}
              onChange={(e) => this.handleChange(e)}
            />
            <button
              onClick={() => this.handlesubmit()}
              style={{ background: "red", color: "white", padding: 5 }}
            >
              create list
            </button>
          </Box>
        </Flex>
      </Box>
    );
  }
}
