import React, { Component } from 'react'
import { getChecklist,createCheckList } from '../api'
import { Box, Heading ,Checkbox,Input,Button,ListItem,ListIcon, List} from '@chakra-ui/react'
import { SettingsIcon} from '@chakra-ui/icons'

export default class CheckList extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
       checklist:[]
    }
  }
  componentDidMount=()=>{
    let id=this.props.data.id
    getChecklist(id).then(data=>{
      console.log(data);
      this.setState({
        checklist:data
      })
    }).catch(err=>console.log(err))
  }
  handleCreate=(e)=>{
    e.preventDefault()
    let {data}=this.props
    let name=(e.target['0'].value);
    createCheckList(data.id,name).then(() => {
      getChecklist(data.id).then((res) => {
        this.setState({
          checklist: res,
        });
      });
    });
  }
  render() {
    let {data}=this.props
    let {checklist}=this.state
    console.log(checklist,'vvvvv');
    return (
      <>
      <Heading as='h2' size='3xl' m="10">
    {data.name}
  </Heading>
      <div className='left-check'>
      <Heading as='h3' size='lg'>
    CheckList
  </Heading>
       {
        checklist.map(list=>(
          <Box bg='grey' w='100%' p={4} color='white'>
          <Checkbox >{list.name}</Checkbox>
          {list.checkItems.length>0?list.checkItems.map((item)=>(
            <List>
            <ListItem>
            <ListIcon as={SettingsIcon} color='maroon' />
            {item.name}
          </ListItem>
          </List>
          )):""}
        </Box>
        ))
       }
      </div>
      <form onSubmit={this.handleCreate}>
            <Input name="boardName" placeholder="Type the checklist name" size="lg" />
            <Button
             type="submit"
              m=".8rem"
              colorScheme="teal"
              variant="solid"
            >
                Create new checklist
            </Button>
            </form>
      </>
    )
  }
}
