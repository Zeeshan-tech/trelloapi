import React, { Component } from "react";
import { getCards, createCard, deleteCard } from "../api";
import { VStack, Box, StackDivider,Badge, Flex } from "@chakra-ui/react";
import { DeleteIcon } from '@chakra-ui/icons'
import Modal from 'react-modal'
import CheckList from "./CheckList";
export default class DisplayCards extends Component {
  constructor(props) {
    super(props);

    this.state = {
      cardDetails: [],
      newcard: "",
      openidx:-1
    };
  }
  componentDidMount() {
    let { id } = this.props;
    getCards(id).then((data) =>
      this.setState({
        cardDetails: data,
      })
    );
  }
  handleSubmit = () => {
    let { id } = this.props;
    let { newcard } = this.state;
    createCard(id, newcard).then(() => {
      let { id } = this.props;
      getCards(id).then((data) =>
        this.setState({
          cardDetails: data,
          newcard:"",
          openidx:-1

        })
      );
    });
  };
  handleCardClick=(e,idx)=>{
    console.log('card clickedd',idx);
    this.setState({
      openidx:idx
    })
  }

  handleClose=()=>{
    this.setState({
      openidx:-1
    })
    return false
  }
  handleCardDel=(id)=>{
    let {cardDetails}=this.state
    deleteCard(id).then(()=>{
      // let del=cardDetails.filter((cards)=>cardDetails.id!==id)
      // this.setState({
      //   ...this.state,
      //   cardDetails:del
      // })
      let { id } = this.props;
    getCards(id).then((data) =>
      this.setState({
        cardDetails: data,
      })
    );
    })
  }
  render() {
    let { cardDetails, newcard ,openidx} = this.state;
    return (
      <div>
        <VStack
          divider={<StackDivider borderColor="gray.200" />}
          spacing={4}
          align="stretch"
        >
          {cardDetails.map((data,idx) => {
            
            return(<>
            <Flex>
            <Box key={idx} w="95%" m="0 auto" onClick={()=>this.setState({
              openidx:idx
            })} >
              
              <Badge p="2" colorScheme='green'>{data.name}</Badge>
            </Box>
            <DeleteIcon onClick={()=>this.handleCardDel(data.id)}/>
            </Flex>
            <Modal isOpen={openidx===idx?true:false}  style={{content:{width:500,margin:'0 auto',background:'skyblue'}}} className="model"  >
                <button style={{fontSize:'2rem'}} value={idx} onClick={(e)=>this.handleClose(e)} className="close">X</button>
                <CheckList data={data}/>
             </Modal>
            </>
          )})}
        
          <input
            placeholder="add card"
            type="text"
            value={newcard}
            onChange={(e) => this.setState({ newcard: e.target.value })}
          />
          <button style={{ background: "skyblue", color: "maroon", padding: 5 }} onClick={this.handleSubmit} >create card</button>
        </VStack>
      </div>
    );
  }
}
