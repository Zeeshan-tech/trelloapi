import React, { Component } from "react";
import {
  Badge,
  Text,
  HStack,
  Box,
  Heading,
  Flex,
  Input,
  Button
} from "@chakra-ui/react";
import { getBoards, createBoard } from "../api";
import { Link } from "react-router-dom";
export default class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      boards: [],
    };
  }
  componentDidMount = () => {
    getBoards().then((boards) => {
      this.setState({
        boards,
      });
    });
  };
  handleCreate = (e) => {
    e.preventDefault()
    let name=(e.target['0'].value);
    createBoard(name).then(() => {
      getBoards().then((data) => {
        this.setState({
          boards: data,
        });
      });
    });
  };
  render() {
    let { boards } = this.state;
    return (
      <div style={{ width: "80%", margin: "0 auto" }}>
        <HStack align="center" spacing="1.5rem">
          <Badge fontSize="1.5rem" variant="solid" colorScheme="green">
            T
          </Badge>
          <Text fontSize="3xl">trello Workspace</Text>
        </HStack>
        <Flex wrap="wrap">
          <Box
          boxShadow='dark-lg'
            
            w="300px"
            p={2}
            h="150px"
            color="grey"
            m=".8rem"
            borderRadius="8px"
          > <form onSubmit={this.handleCreate}>
            <Input name="boardName" placeholder="Type the board name" size="lg" />
            <Button
             type="submit"
              m=".8rem"
              colorScheme="teal"
              variant="solid"
            >
                Create new Board
            </Button>
            </form>
          </Box>
          {boards.map((board) => (
            <Link to={`/board/${board.id}`}>
              <Box
                id={board.id}
                w="300px"
                p={2}
                h="150px"
                bg="maroon"
                color="white"
                m=".8rem"
                borderRadius="8px"
                backgroundImage={`url(${board.prefs.backgroundImage})`}
                backgroundSize="300px"
                style={{ objectFit: "contain" }}
              >
                <Heading>{board.name}</Heading>
              </Box>
            </Link>
          ))}
        </Flex>
      </div>
    );
  }
}
