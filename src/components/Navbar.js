import React, { Component } from 'react'
import { HamburgerIcon } from '@chakra-ui/icons'
import { Icon } from '@chakra-ui/react'
import { BsHouseDoor,BsSearch ,BsPlusLg,BsArrowUpRightCircle,BsQuestionCircle,BsFillBellFill} from "react-icons/bs";
import { Input } from '@chakra-ui/react'
export default class Navbar extends Component {
  render() {
    let {setSidemenu,sidemenu}=this.props
    return (
      <div className='nav flex'>
        <div className='left-nav'>
          <HamburgerIcon className='icon' onClick={()=>setSidemenu(!sidemenu)}/>
           <Icon className='icon' as={BsHouseDoor}/>
        <div className='search-box flex'>
        <Icon className='icon' as={BsSearch}/>
        <Input  placeholder='Search' />
        </div>
        </div>
        <div className='right-nav'>
        <Icon className='icon' as={BsPlusLg}/>
        <Icon className='icon' as={BsArrowUpRightCircle}/>
        <Icon className='icon' as={BsQuestionCircle}/>
        <Icon className='icon' as={BsFillBellFill}/>
        <div className='name'>z</div>
        </div>
      </div>
    )
  }
}
