import React, { Component } from 'react'
import Sidebox from './Sidebox'
import {BsInbox,BsCalendarDate,BsCalendar3,BsXDiamond} from "react-icons/bs";
export default class Sidebar extends Component {
  render() {
    return (
      <div className="side-bar">
        <Sidebox content="Inbox" icon={BsInbox} count={5} />
        <Sidebox content="Today" icon={BsCalendarDate} count={5} />
        <Sidebox content="Upcoming" icon={BsCalendar3} count={5} />
        <Sidebox content="Filters & Labels" icon={BsXDiamond} count={5} />
      </div>
    )
  }
}
