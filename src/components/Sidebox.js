import React, { Component } from 'react'
import { Icon } from '@chakra-ui/react'

export default class Sidebox extends Component {
  render() {
    let {content,icon,count}=this.props
    return (
      <div className='flex side-box'>    
              <div className='flex vcenter'>
               <Icon className='icon' as={icon}/>
               <div>{content}</div>
               </div>
               <div>
                {count}
               </div>
      </div>
    )
  }
}
